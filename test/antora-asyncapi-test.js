/* eslint-env mocha */
'use strict'

const EventEmitter = require('events')
const antoraAsyncapi = require('./../lib/antora-asyncapi')
const events = require('./../lib/constants')
const mockContentCatalog = require('./mock-content-catalog')
const { expect } = require('chai')

const seed = [
  {
    relative: 'page.adoc',
    contents: `= Fabulous page
    
    Once upon a time,
`,
  },
]

describe('antora-asyncapi tests', () => {
  var eventEmitter

  function setUpEventEmitter () {
    const baseEmitter = new EventEmitter()

    eventEmitter = {

      emit: async (name, ...args) => {
        const promises = []
        baseEmitter.emit(name, promises, ...args)
        promises.length && await Promise.all(promises)
      },

      on: (name, listener) => baseEmitter.on(name, (promises, ...args) => promises.push(listener(...args))),
    }
  }
  beforeEach(() => {
    setUpEventEmitter()
  })

  it('validation error', async () => {
    antoraAsyncapi.register(eventEmitter)
    const contentCatalog = mockContentCatalog([
      {
        family: 'example',
        relative: 'test.yml',
        contents: `asyncapi: '2.0.0'

externalDocs:
  description: Find more info here
  url: https://www.asyncapi.com

info:
  title: Dummy example with all spec features included
  version: '0.0.1'
  description: |
    This is an example of AsyncAPI specification file that is suppose to include all possible features of the AsyncAPI specification. Do not use it on production.

    It's goal is to support development of documentation and code generation with the [AsyncAPI Generator](https://github.com/asyncapi/generator/) and [Template projects](https://github.com/search?q=topic%3Aasyncapi+topic%3Agenerator+topic%3Atemplate)
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  contact:
    name: API Support
    url: http://www.asyncapi.com/support
    email: info@asyncapi.io
  x-twitter: '@AsyncAPISpec'

`,
      },
    ])
    const messages = await captureStdOut(eventEmitter.emit, events.AFTER_CLASSIFY_CONTENT, {}, contentCatalog)
    expect(messages.length).to.equal(1)
    expect(messages[0]).to.contain('could not generate asyncapi document master@component-a:module-a:page$test.adoc')
    expect(messages[0]).to.contain('should have required property ')
    expect(messages[0]).to.contain('channels')
  })

  it('url test', async () => {
    antoraAsyncapi.register(eventEmitter, {
      urls: [{
        url: 'test/fixtures/asyncapi-test.yml',
        component: 'component-a',
        version: 'master',
        module: 'asyncapi',
        relative: 'asyncapi-test.adoc',
      }],
    })
    const contentCatalog = mockContentCatalog(seed)
    console.log('about to try fs access')
    const messages = await captureStdOut(eventEmitter.emit, events.AFTER_CLASSIFY_CONTENT, {}, contentCatalog)
    // console.log(`messages.length: ${messages.length}`)
    // messages.forEach((message) => console.log(message))
    if (messages.length === 1) {
      // First time, there's an installation message:
      expect(messages[0]).to.contain('@djencks/asyncapi-asciidoc-template@0.0.1')
    } else {
      expect(messages.length).to.equal(0)
    }
  }).timeout(5000)

  it('url no component-version test', async () => {
    antoraAsyncapi.register(eventEmitter, {
      urls: [{
        url: 'test/fixtures/asyncapi-test.yml',
        component: 'component-a',
        version: 'v2.0',
        module: 'asyncapi',
        relative: 'asyncapi-test.adoc',
      }],
    })
    const contentCatalog = mockContentCatalog(seed)
    const messages = await captureStdOut(eventEmitter.emit, events.AFTER_CLASSIFY_CONTENT, {}, contentCatalog)
    // console.log(`messages.length: ${messages.length}`)
    // messages.forEach((message) => console.log(message))
    expect(messages.length).to.equal(1)
    if (messages.length === 1) {
      // First time, there's an installation message:
      expect(messages[0]).to.equal('No component component-a at version v2.0 for url test/fixtures/asyncapi-test.yml: skipping')
    } else {
      expect(messages.length).to.equal(0)
    }
  }).timeout(5000)
})

async function captureStdOut (fn, ...args) {
  const stdOutWrite = process.stderr.write
  const messages = []
  try {
    process.stdout.write = (msg) => messages.push(msg.trim())
    await fn(...args)
    return messages
  } finally {
    process.stdout.write = stdOutWrite
  }
}
