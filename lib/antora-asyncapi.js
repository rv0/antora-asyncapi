'use strict'

const events = require('./constants')
const expandPath = require('@antora/expand-path-helper')
const { promises: fsp } = require('fs')
const Generator = require('@asyncapi/generator')
const parser = require('@asyncapi/parser')
const get = require('got')
const picomatch = require('picomatch')

const templateLocation = require('@djencks/asyncapi-asciidoc-template')()

const DEFAULT_NAV_INDEX = 10
const URI_SCHEME_RX = /^https?:\/\//

module.exports.register = (eventEmitter, config = {}) => {
  //config is from playbook entry for this extension.
  // Config contains a queries array of component/version/module filters and picomatch relative matches.
  const queries = (config && config.queries) || ((config && config.urls) ? [] : [{}])
  // Config contains a urls array of url/component/version/module/relative specifications.
  const urls = (config && config.urls) || []
  const bodyByTag = 'bodyByTag' in config
  const renderToc = 'renderToc' in config
  const generateNav = 'generateNav' in config
  const navByTag = 'navByTag' in config
  const tocByTag = 'tocByTag' in config
  const timing = 'timing' in config

  eventEmitter.on(events.AFTER_CLASSIFY_CONTENT,
    async (playbook, contentCatalog) => {
      const startDir = playbook.dir || '.'
      // console.log('templateLocation', templateLocation)
      const beforeLoad = Date.now()
      const generator = new Generator(templateLocation,
        null,
        {
          templateParams: { renderToc, tocByTag, bodyByTag },
          entrypoint: 'asyncapi.adoc',
          output: 'string',
        })
      timing && console.log(`loading generator took ${Date.now() - beforeLoad} ms`)

      const pages = Object.values(queries.reduce((accum, query) => {
        query = Object.assign({}, query, { family: 'example' })
        const relative = query.relative
        const relCompare = relative ? (page) => picomatch(relative)(page.src.relative) : () => true
        delete query.relative
        const navIndex = query.navIndex || DEFAULT_NAV_INDEX
        delete query.navIndex
        contentCatalog.findBy(query).filter(relCompare).forEach((file) => {
          const key = generateKey(file)
          if (!(key in accum)) accum[key] = [file, navIndex]
        })
        return accum
      }, {}))
      const beforeGenerateAll = Date.now()
      await Promise.all([
        Promise.all(pages.map(async ([source, navIndex]) => {
          const src = Object.assign({}, source.src, { family: 'page', mediaType: 'text/asciidoc' })
          src.relative = src.relative.slice(0, src.relative.length - src.extname.length) + '.adoc'
          src.extname = '.adoc'
          const dirname = source.dirname
          const path = source.path
          const sourceString = source.contents.toString()
          return await generate(sourceString, generator, src, dirname, path, contentCatalog, navIndex)
        })),
        Promise.all(urls.map(
          async ({ url, component, version, module: module_, relative, navIndex = DEFAULT_NAV_INDEX }) => {
            if (contentCatalog.getComponentVersion(component, version)) {
              try {
                const src = {
                  component,
                  version,
                  module: module_,
                  relative,
                  family: 'page',
                  mediaType: 'text/asciidoc',
                }
                const dirname = `modules/${module_}/pages`
                const path = `modules/${module_}/pages/${relative}`
                let source
                if (isUrl(url)) {
                  source = await get(url, { resolveBodyOnly: true, responseType: 'buffer' })
                } else {
                  const localPath = expandPath(url, '~+', startDir)
                  source = await fsp.readFile(localPath)
                }
                return await generate(source.toString(), generator, src, dirname, path, contentCatalog, navIndex)
              } catch (e) {
                return console.log(`could not read asyncapi document from url ${url}`, e)
              }
            } else {
              console.log(`No component ${component} at version ${version} for url ${url}: skipping`)
            }
          })),
      ])
      timing && console.log(`total generation time ${Date.now() - beforeGenerateAll}`)
    }
  )

  async function generate (sourceString, generator, src, dirname, path, contentCatalog, navIndex) {
    try {
      const beforeGenerate = Date.now()
      const parsed = await parser.parse(sourceString)
      timing && console.log(`parsing ${generateKey(src)} took ${Date.now() - beforeGenerate} ms`)
      const asciidoc = await generator.generate(parsed)
      const page = {
        src,
        dirname,
        path,
        mediaType: 'text/asciidoc',
        contents: Buffer.from(asciidoc),
      }
      if (generateNav) {
        const navGenerator = new Generator(templateLocation,
          null,
          {
            templateParams: {
              prefix: `${src.version}@${src.component}:${src.module}:${src.relative}`,
              outFilename: '',
              navByTag,
            },
            entrypoint: 'nav.adoc',
            output: 'string',
          })
        const navAsciidoc = await navGenerator.generate(parsed)
        // console.log('navAsciidoc:', navAsciidoc)
        const nav = {
          src: Object.assign({}, src, { family: 'nav' }),
          dirname,
          path,
          mediaType: 'text/asciioc',
          contents: Buffer.from(navAsciidoc),
          nav: { index: navIndex },
        }
        contentCatalog.addFile(nav)
      }
      timing && console.log(`generating ${generateKey(src)} took ${Date.now() - beforeGenerate} ms`)
      return contentCatalog.addFile(page)
    } catch (e) {
      return console.log(`could not generate asyncapi document ${generateKey(src)}`, e.validationErrors ? e.validationErrors : e)
    }
  }
}

function generateKey ({ component, version, module: module_, family, relative }) {
  return `${version}@${component}:${module_}:${family}$${relative}`
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}
